Please do. I know very little about this stuff. Even so, I am very opinionated,
so it's best to ask before starting to work on something, just in case I
disagree with the direction.