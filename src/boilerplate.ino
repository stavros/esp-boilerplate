#include "Main.h"
#include "settings.h"

#ifndef WIFI_SSID
#define WIFI_ENABLED 0
#else
#define WIFI_ENABLED 1
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

WiFiClient wclient;
#endif

#ifndef OTA_PASS
#define OTA_ENABLED 0
#else
#define OTA_ENABLED 1
#endif

#ifndef PROJECT_NAME
#define PROJECT_NAME "sensors"
#endif

#ifndef INSTANCE_NAME
#define INSTANCE_NAME "main"
#endif

#ifndef MQTT_SERVER
#define MQTT_ENABLED 0
#else
#define MQTT_ENABLED 1
#include "PubSubClient.h"

char COMMAND_TOPIC[] = PROJECT_NAME "/" INSTANCE_NAME "/command";
char LOG_TOPIC[] = PROJECT_NAME "/" INSTANCE_NAME "/log";

#endif

#ifndef NTP_SERVER
#define NTP_ENABLED 0
#else
#define NTP_ENABLED 1
#endif

unsigned long bootTime = 0;

Main main;

#if MQTT_ENABLED == 1
PubSubClient client(wclient);

// Publish a message to MQTT if connected.
void mqttPublish(String topic, String payload) {
    if (!client.connected()) {
        return;
    }
    client.publish(topic.c_str(), payload.c_str());
}

// Log something to the log topic.
void mqttLog(String topic, String payload) {
    char chPayload[500];
    char chTopic[100];

    if (!client.connected()) {
        return;
    }

    topic.toCharArray(chTopic, 100);
    ("(" + String(millis()) + " - " + WiFi.localIP().toString() + ") " + INSTANCE_NAME + ": " + payload).toCharArray(chPayload, 100);
    client.publish(chTopic, chPayload);
}


// Receive a message from MQTT and act on it.
void mqttCallback(char* chTopic, byte* chPayload, unsigned int length) {
    chPayload[length] = '\0';
    String payload = String((char*)chPayload);
    //TODO: Hook here, with payload.
}
#endif


#if NTP_ENABLED == 1
unsigned long getNTPTime() {
    IPAddress address;
    WiFiUDP udp;
    const int NTP_PACKET_SIZE = 48;
    byte packetBuffer[NTP_PACKET_SIZE];

    udp.begin(2390);
    // set all bytes in the buffer to 0
    memset(packetBuffer, 0, NTP_PACKET_SIZE);
    // Initialize values needed to form NTP request
    // (see URL above for details on the packets)
    packetBuffer[0] = 0b11100011;   // LI, Version, Mode
    packetBuffer[1] = 0;     // Stratum, or type of clock
    packetBuffer[2] = 6;     // Polling Interval
    packetBuffer[3] = 0xEC;  // Peer Clock Precision
    // 8 bytes of zero for Root Delay & Root Dispersion
    packetBuffer[12]  = 49;
    packetBuffer[13]  = 0x4E;
    packetBuffer[14]  = 49;
    packetBuffer[15]  = 52;

    // all NTP fields have been given values, now
    // you can send a packet requesting a timestamp:
    WiFi.hostByName(NTP_SERVER, address);
    udp.beginPacket(address, 123); //NTP requests are to port 123
    udp.write(packetBuffer, NTP_PACKET_SIZE);
    udp.endPacket();

    delay(1000);

    int cb = udp.parsePacket();
    if (!cb) {
        return 0;
    } else {
        udp.read(packetBuffer, NTP_PACKET_SIZE);
        unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
        unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
        unsigned long secsSince1900 = highWord << 16 | lowWord;
        const unsigned long seventyYears = 2208988800UL;
        unsigned long epoch = secsSince1900 - seventyYears;
        return epoch;
    }
}
#endif


void resetPins() {
    pinMode(1, OUTPUT);
    digitalWrite(1, LOW);
    pinMode(2, OUTPUT);
    digitalWrite(2, LOW);
    pinMode(3, OUTPUT);
    digitalWrite(3, LOW);
    pinMode(4, OUTPUT);
    digitalWrite(4, LOW);
    pinMode(5, OUTPUT);
    digitalWrite(5, LOW);
    pinMode(12, OUTPUT);
    digitalWrite(12, LOW);
    pinMode(13, OUTPUT);
    digitalWrite(13, LOW);
    pinMode(14, OUTPUT);
    digitalWrite(14, LOW);
    pinMode(15, OUTPUT);
    digitalWrite(15, LOW);
}


unsigned long currentTime() {
    return bootTime + (millis() / 1000);
}


// Check the MQTT connection and reboot if we can't connect.
#if MQTT_ENABLED == 1
void connectMQTT() {
    if (client.connected()) {
        client.loop();
    } else {
        int retries = 4;
        Serial.println("\nConnecting to MQTT...");
        while (!client.connect(INSTANCE_NAME) && retries--) {
            delay(500);
            Serial.println("Retry...");
        }

        if (!client.connected()) {
            Serial.println("\nfatal: MQTT server connection failed. Rebooting.");
            delay(200);
            ESP.restart();
        }

        Serial.println("Connected.");
        client.subscribe(COMMAND_TOPIC);
    }
}
#endif


#if WIFI_ENABLED == 1
void connectWifi() {
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        WiFi.mode(WIFI_STA);
        while (WiFi.waitForConnectResult() != WL_CONNECTED) {
            WiFi.begin(WIFI_SSID, WIFI_PASS);
            Serial.println("Connecting to wifi...");
        }

        Serial.print("Wifi connected, IP address: ");
        Serial.println(WiFi.localIP());
    }
}
#endif


void setup() {
    resetPins();
    Serial.begin(115200);

#if MQTT_ENABLED == 1
    client.setServer(MQTT_SERVER, MQTT_PORT);
    client.setCallback(mqttCallback);
#endif

#if WIFI_ENABLED == 1
    connectWifi();
#endif

#if MQTT_ENABLED == 1
    connectMQTT();
#endif

#if OTA_ENABLED == 1
    // Enable OTA updates.
    ArduinoOTA.setPassword((const char *) OTA_PASS);
    ArduinoOTA.setHostname("ESPlights");

    ArduinoOTA.onStart([]() {
        Serial.println("Start");
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
            Serial.println("Auth Failed");
        } else if (error == OTA_BEGIN_ERROR) {
            Serial.println("Begin Failed");
        } else if (error == OTA_CONNECT_ERROR) {
            Serial.println("Connect Failed");
        } else if (error == OTA_RECEIVE_ERROR) {
            Serial.println("Receive Failed");
        } else if (error == OTA_END_ERROR) {
            Serial.println("End Failed");
        }
    });
    ArduinoOTA.begin();
#endif

    main.setup();
}


void loop() {
#if WIFI_ENABLED == 1
    connectWifi();
#endif

#if MQTT_ENABLED == 1
    connectMQTT();
#endif

#if OTA_ENABLED == 1
    ArduinoOTA.handle();
#endif
    main.loop();
}
