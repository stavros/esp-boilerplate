ESP8266-boilerplate
===================

[![build
status](https://gitlab.com/skorokithakis/esp-boilerplate/badges/master/build.svg)](https://gitlab.com/skorokithakis/esp-boilerplate/commits/master)

ESP8266-boilerplate is a boilerplate library. It will save you from writing the
same code over and over again. For example, if you want a straightforward setup
where the ESP connects to a Wifi access point and an MQTT server and talks to
NTP to get the current time, just add the relevant settings (Wifi SSID,
password, MQTT server hostname/port, NTP server hostname) to the `settings.h`
file, and you're good to go, without writing a single extra line of code!

## Usage

To use ESP8266-boilerplate, just copy `settings.h.example` to `settings.h`. Do
not commit this file, as it may contain secrets. Then, open `Main.cpp` and write
your code in the `setup()` and `loop()` methods, and you're done!

There are also various callbacks available, which I will detail just as soon as
I figure out how to write them.

## Settings

To see the available settings, look in `settings.h.example`.

## License

BSD 3-clause. Go nuts!
